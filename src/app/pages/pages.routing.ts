import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../guards/auth.guard';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChatComponent } from './chat/chat.component';
import { ClientesComponent } from './clientes/clientes.component';
import { AgentesComponent } from './agentes/agentes.component';
import { ProgressComponent } from './progress/progress.component';
import { Grafica1Component } from './grafica1/grafica1.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { PerfilComponent } from './perfil/perfil.component';

// Mantenimientos
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { ServicioCrearComponent } from './servicios/servicio-crear/servicio-crear.component';
import { ServicioEditarComponent } from './servicios/servicio-editar/servicio-editar.component';
import { FaqComponent } from './faq/faq.component';
import { FaqCrearComponent } from './faq/faq-crear/faq-crear.component';
import { FaqService } from '../services/faq.service';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/crear-usuario.component';
import { EditarUsuarioComponent } from './usuarios/editar-usuario/editar-usuario.component';

import { CrearClienteComponent } from './clientes/crear-cliente/crear-cliente.component';
import { EditarClienteComponent } from './clientes/editar-cliente/editar-cliente.component';

import { CrearAgenteComponent } from './agentes/crear-agente/crear-agente.component';
import { EditarAgenteComponent } from './agentes/editar-agente/editar-agente.component';


const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        canActivate: [ AuthGuard ],
        children: [
            { path: '', component: DashboardComponent, data: { titulo: 'Dashboard' } },
            { path: 'progress', component: ProgressComponent, data: { titulo: 'ProgressBar' }},
            { path: 'grafica1', component: Grafica1Component, data: { titulo: 'Gráfica #1' }},
            { path: 'account-settings', component: AccountSettingsComponent, data: { titulo: 'Ajustes de cuenta' }},
            { path: 'promesas', component: PromesasComponent, data: { titulo: 'Promesas' }},
            { path: 'rxjs', component: RxjsComponent, data: { titulo: 'RxJs' }},
            { path: 'perfil', component: PerfilComponent, data: { titulo: 'Perfil de usuario' }},
            { path: 'servicios', component: ServiciosComponent, data: { titulo: 'Servicios' }},
            { path: 'servicios/crear', component: ServicioCrearComponent, data: { titulo: 'Servicios' }},
            { path: 'servicios/editar/:id', component: ServicioEditarComponent, data: { titulo: 'Servicios' }},
            { path: 'faqs', component: FaqComponent, data: { titulo: 'Faqs' }},
            { path: 'faqs/crear', component: FaqCrearComponent, data: { titulo: 'Faqs' }},
            { path: 'faqs/editar/:id', component: FaqService, data: { titulo: 'Faqs' }},
            { path: 'chats', component: ChatComponent, data: { titulo: 'Chat de aplicación' }},
            // Mantenimientos
            { path: 'usuarios', component: UsuariosComponent, data: { titulo: 'Usuario' }},
            { path: 'usuarios/crear', component: CrearUsuarioComponent, data: { titulo: 'Usuario' }},
            { path: 'usuarios/editar/:id', component: EditarUsuarioComponent, data: { titulo: 'Usuario' }},
            { path: 'clientes', component: ClientesComponent, data: { titulo: 'Clientes' }},
            { path: 'clientes/crear', component: CrearClienteComponent, data: { titulo: 'Clientes' }},
            { path: 'clientes/editar/:id', component: EditarClienteComponent, data: { titulo: 'Clientes' }},
            { path: 'agentes', component: AgentesComponent, data: { titulo: 'Agentes' }},
            { path: 'agentes/crear', component: CrearAgenteComponent, data: { titulo: 'Agentes' }},
            { path: 'agentes/editar/:id', component: EditarAgenteComponent, data: { titulo: 'Agentes' }},
        ]
    },
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class PagesRoutingModule {}


