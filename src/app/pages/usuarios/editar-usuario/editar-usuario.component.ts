import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Usuario } from 'src/app/models/usuario.model';
import { UsuarioService } from 'src/app/services/usuario.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FileUploadService } from 'src/app/services/file-upload.service';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {
  id: string;
  user: Usuario = {
    name: '',
    email: '',
    img: '',
    type: 1,
    services: [],
    uid: '',
    imagenUrl: ''
  };
  public formSubmitted = false;
  public imagenSubir: File;
  public imgTemp: any = null;

  public usuarioForm = this.fb.group({
    name: [ '', Validators.required ],
    email: ['', Validators.required ],
    password: ['', Validators.required ],
    status: ['', Validators.required ],
  });


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private usuarioService: UsuarioService,
    private fb: FormBuilder,
    private fileUploadService: FileUploadService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.getById(this.id);
    });
  }
  getById(id) {
    this.usuarioService.findById(id).subscribe(resp => {
      this.user = resp.user;
      console.log(this.user);
    });
  }
  edit() {
    this.usuarioService.edit( this.id, this.usuarioForm.value )
      .subscribe( resp => {
        if(this.imgTemp) {
          this.fileUploadService
            .actualizarFoto( this.imagenSubir, 'users', resp.data.uid )
            .then( img => {
              console.log(img);
            }).catch( err => {
              console.log(err);
              Swal.fire('Error', 'No se pudo subir la imagen', 'error');
            })
        }
        Swal.fire('Usuario Editado', 'success' );
        this.router.navigateByUrl('/usuarios');
      }, (err) => {
        Swal.fire('Error', err.error.msg, 'error' );
      });
  }

  cambiarImagen( file: File ) {
    this.imagenSubir = file;

    if ( !file ) { 
      return this.imgTemp = null;
    }

    const reader = new FileReader();
    reader.readAsDataURL( file );

    reader.onloadend = () => {
      this.imgTemp = reader.result;
    }
  }
}
