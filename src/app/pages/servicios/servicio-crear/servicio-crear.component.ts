import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServicioService } from '../../../services/servicio.service';

import Swal from 'sweetalert2';
import { FileUploadService } from 'src/app/services/file-upload.service';

@Component({
  selector: 'app-servicio-crear',
  templateUrl: './servicio-crear.component.html',
  styleUrls: ['./servicio-crear.component.css']
})
export class ServicioCrearComponent implements OnInit {

  public formSubmitted = false;
  public imagenSubir: File;
  public imgTemp: any = null;

  // TODO check for show validation msg

  public servicioForm = this.fb.group({
    name: [ '', Validators.required ],
    type: ['', Validators.required ],
    value: ['', Validators.required ],
    status: ['', Validators.required ],
    description: ['', Validators.required ],
  });

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private servicioService: ServicioService,
    private fileUploadService: FileUploadService
  ) { }

  ngOnInit(): void {
  }

  create() {
    this.servicioService.create( this.servicioForm.value )
      .subscribe( resp => {
        console.log(resp);
        if(this.imgTemp) {
          this.fileUploadService
            .actualizarFoto( this.imagenSubir, 'services', resp.data.uid )
            .then( img => {
              console.log(img);
            }).catch( err => {
              console.log(err);
              Swal.fire('Error', 'No se pudo subir la imagen', 'error');
            })
        }
        Swal.fire('Servicio Creado', 'success' );
        this.router.navigateByUrl('/servicios');
      }, (err) => {
        Swal.fire('Error', err.error.msg, 'error' );
      });
  }

  cambiarImagen( file: File ) {
    this.imagenSubir = file;

    if ( !file ) { 
      return this.imgTemp = null;
    }

    const reader = new FileReader();
    reader.readAsDataURL( file );

    reader.onloadend = () => {
      this.imgTemp = reader.result;
    }

  }

}
