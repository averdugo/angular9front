import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Servicio } from 'src/app/models/servicio.model';
import { ServicioService } from 'src/app/services/servicio.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FileUploadService } from 'src/app/services/file-upload.service';

@Component({
  selector: 'app-servicio-editar',
  templateUrl: './servicio-editar.component.html',
  styleUrls: ['./servicio-editar.component.css']
})
export class ServicioEditarComponent implements OnInit {
  id: string;
  service: Servicio = {
    name: '',
    type: 0,
    description: '',
    value: 0,
    status: 0,
    imagenUrl: ''
  };
  public formSubmitted = false;
  public imagenSubir: File;
  public imgTemp: any = null;


  public servicioForm = this.fb.group({
    name: [ '', Validators.required ],
    type: ['', Validators.required ],
    value: ['', Validators.required ],
    status: ['', Validators.required ],
    description: ['', Validators.required ],
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private servicioService: ServicioService,
    private fb: FormBuilder,
    private fileUploadService: FileUploadService
    ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.getById(this.id);
    });
  }

  getById(id) {
    this.servicioService.findById(id).subscribe(resp => {
      this.service = resp.data;
      console.log(this.service);
    });
  }
  edit() {
    this.servicioService.edit( this.id, this.servicioForm.value )
      .subscribe( resp => {
        if(this.imgTemp) {
          this.fileUploadService
            .actualizarFoto( this.imagenSubir, 'services', resp.data.uid )
            .then( img => {
              console.log(img);
            }).catch( err => {
              console.log(err);
              Swal.fire('Error', 'No se pudo subir la imagen', 'error');
            })
        }
        Swal.fire('Servicio Editado', 'success' );
        this.router.navigateByUrl('/servicios');
      }, (err) => {
        Swal.fire('Error', err.error.msg, 'error' );
      });
  }

  cambiarImagen( file: File ) {
    this.imagenSubir = file;

    if ( !file ) { 
      return this.imgTemp = null;
    }

    const reader = new FileReader();
    reader.readAsDataURL( file );

    reader.onloadend = () => {
      this.imgTemp = reader.result;
    }

  }

}
