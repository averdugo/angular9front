import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import Swal from 'sweetalert2';
import { Servicio } from '../../models/servicio.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.css']
})
export class ServiciosComponent implements OnInit, OnDestroy {
  base_url = environment.base_url;
  services: [];
  cargando = false;
  public totalServicios = 0;
  private sub: any;

  constructor(
    public servicioService: ServicioService
  ) {}

  ngOnInit(): void {
    this.getAll();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  getAll(){
    this.sub = this.servicioService.all().subscribe( resp  => {
      this.totalServicios = resp.data.length;
      this.services = resp.data;
    }, (err) => {
      Swal.fire('Error', err.error.msg, 'error' );
    });
  }

  buscar(txt){}

  delete( servicio: Servicio ) {
    Swal.fire({
      title: '¿Borrar servicio?',
      text: `Esta a punto de borrar a ${ servicio.name }`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Si, borrarlo'
    }).then((result) => {
      if (result.value) {
        this.servicioService.delete( servicio )
          .subscribe( resp => {
            if (!resp.ok) {
              Swal.fire( 'Error', resp.msg, 'error');
            }
            this.getAll();
            Swal.fire(
              'Servicio borrado',
              `${ servicio.name } fue eliminado correctamente`,
              'success'
            );
          });
      }
    });
  }

  // TODO modal cambiar imagen service

}
