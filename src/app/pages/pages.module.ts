import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditorModule } from '@tinymce/tinymce-angular';

// Modulos
import { SharedModule } from '../shared/shared.module';
import { ComponentsModule } from '../components/components.module';


import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Grafica1Component } from './grafica1/grafica1.component';
import { PagesComponent } from './pages.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { PerfilComponent } from './perfil/perfil.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { ServicioCrearComponent } from './servicios/servicio-crear/servicio-crear.component';
import { ServicioEditarComponent } from './servicios/servicio-editar/servicio-editar.component';
import { FaqEditarComponent } from './faq/faq-editar/faq-editar.component';
import { FaqComponent } from './faq/faq.component';
import { FaqCrearComponent } from './faq/faq-crear/faq-crear.component';
import { ChatComponent } from './chat/chat.component';
import { ClientesComponent } from './clientes/clientes.component';
import { AgentesComponent } from './agentes/agentes.component';
import { CrearClienteComponent } from './clientes/crear-cliente/crear-cliente.component';
import { EditarClienteComponent } from './clientes/editar-cliente/editar-cliente.component';
import { EditarUsuarioComponent } from './usuarios/editar-usuario/editar-usuario.component';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/crear-usuario.component';
import { CrearAgenteComponent } from './agentes/crear-agente/crear-agente.component';
import { EditarAgenteComponent } from './agentes/editar-agente/editar-agente.component';



@NgModule({
  declarations: [
    DashboardComponent,
    ProgressComponent,
    Grafica1Component,
    PagesComponent,
    AccountSettingsComponent,
    PromesasComponent,
    RxjsComponent,
    PerfilComponent,
    UsuariosComponent,
    ServiciosComponent,
    ServicioCrearComponent,
    ServicioEditarComponent,
    FaqEditarComponent,
    FaqComponent,
    FaqCrearComponent,
    ChatComponent,
    ClientesComponent,
    AgentesComponent,
    CrearClienteComponent,
    EditarClienteComponent,
    EditarUsuarioComponent,
    CrearUsuarioComponent,
    CrearAgenteComponent,
    EditarAgenteComponent,
  ],
  exports: [
    DashboardComponent,
    ProgressComponent,
    Grafica1Component,
    PagesComponent,
    AccountSettingsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule,
    ComponentsModule,
    EditorModule
  ]
})
export class PagesModule { }
