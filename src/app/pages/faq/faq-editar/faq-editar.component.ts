import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Faq } from 'src/app/models/faq.model';
import { FaqService } from 'src/app/services/faq.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-faq-editar',
  templateUrl: './faq-editar.component.html',
  styleUrls: ['./faq-editar.component.css']
})
export class FaqEditarComponent implements OnInit {
  id: string;
  faq: Faq = {
    question: '',
    answer: ''
  };

  public formSubmitted = false;

  public faqForm = this.fb.group({
    question: [ '', Validators.required ],
    answer: ['', Validators.required ],
  });


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private faqService: FaqService,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.getById(this.id);
    });
  }

  getById(id) {
    this.faqService.findById(id).subscribe(resp => {
      this.faq = resp.data;
    });
  }
  edit() {
    this.faqService.edit( this.id, this.faqForm.value )
      .subscribe( resp => {
        Swal.fire('Faq Editado', 'success' );
        this.router.navigateByUrl('/faqs');
      }, (err) => {
        Swal.fire('Error', err.error.msg, 'error' );
      });
  }

}
