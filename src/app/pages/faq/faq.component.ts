import { Component, OnInit, OnDestroy } from '@angular/core';
import { FaqService } from 'src/app/services/faq.service';
import { Datainterface } from '../../interfaces/datai.interface';
import Swal from 'sweetalert2';
import { Faq } from '../../models/faq.model';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit, OnDestroy{
  faqs: [];
  cargando = false;
  public totalFaq = 0;
  private sub: any;

  constructor(
    public faqService: FaqService
  ) { }

  ngOnInit(): void {
    this.getAll();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  getAll(){
    this.sub = this.faqService.all().subscribe( resp  => {
      this.totalFaq = resp.data.length;
      this.faqs = resp.data;
    }, (err) => {
      Swal.fire('Error', err.error.msg, 'error' );
    });
  }

  buscar(txt){}

  delete( faq: Faq ) {
    Swal.fire({
      title: '¿Borrar faq?',
      text: `Esta a punto de borrar a ${ faq.question }`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Si, borrarlo'
    }).then((result) => {
      if (result.value) {
        this.faqService.delete( faq )
          .subscribe( resp => {
            if (!resp.ok) {
              Swal.fire( 'Error', resp.msg, 'error');
            }
            this.getAll();
            Swal.fire(
              'Faq borrado',
              `${ faq.question } fue eliminado correctamente`,
              'success'
            );
          });
      }
    });
  }

}
