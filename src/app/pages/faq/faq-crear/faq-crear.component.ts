import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FaqService } from '../../../services/faq.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-faq-crear',
  templateUrl: './faq-crear.component.html',
  styleUrls: ['./faq-crear.component.css']
})
export class FaqCrearComponent implements OnInit {

  public formSubmitted = false;

  public faqForm = this.fb.group({
    question: [ '', Validators.required ],
    answer: ['', Validators.required ],
  });


  constructor(
    private router: Router,
    private fb: FormBuilder,
    private faqService: FaqService,
  ) { }

  ngOnInit(): void {
  }

  create() {
    this.faqService.create( this.faqForm.value )
      .subscribe( resp => {
        Swal.fire('Faq Creado', 'success' );
        this.router.navigateByUrl('/faqs');
      }, (err) => {
        Swal.fire('Error', err.error.msg, 'error' );
      });
  }

}
