import { Component, OnInit } from '@angular/core';
import { UserType } from 'src/app/models/usuario.model';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: [
  ]
})
export class DashboardComponent implements OnInit {
  totalUsuarios=0;
  constructor(
    private usuarioService:UsuarioService
  ) { }

  ngOnInit(): void {
    this.cargarUsuarios()
  }

  cargarUsuarios() {
    this.usuarioService.cargarUsuarios( 0, UserType.Cliente )
      .subscribe( ({ total, usuarios }) => {
        this.totalUsuarios = total;
    })
  }

}
