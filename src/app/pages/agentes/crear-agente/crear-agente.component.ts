import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from '../../../services/usuario.service';
import Swal from 'sweetalert2';
import { FileUploadService } from 'src/app/services/file-upload.service';

@Component({
  selector: 'app-crear-agente',
  templateUrl: './crear-agente.component.html',
  styleUrls: ['./crear-agente.component.css']
})
export class CrearAgenteComponent implements OnInit {

  public formSubmitted = false;
  public imagenSubir: File;
  public imgTemp: any = null;

  public usuarioForm = this.fb.group({
    name: [ '', Validators.required ],
    email: ['', Validators.required ],
    password: ['', Validators.required ],
    status: ['', Validators.required ],
  });

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private fileUploadService: FileUploadService
  ) { }

  ngOnInit(): void {
  }

  create() {
    this.usuarioForm.value.type = 2;

    this.usuarioService.crearUsuario( this.usuarioForm.value )
      .subscribe( resp => {
        console.log(resp);
        if(this.imgTemp) {
          this.fileUploadService
            .actualizarFoto( this.imagenSubir, 'users', resp.data.uid )
            .then( img => {
              console.log(img);
            }).catch( err => {
              console.log(err);
              Swal.fire('Error', 'No se pudo subir la imagen', 'error');
            })
        }
        Swal.fire('Usuario Creado', 'success' );
        this.router.navigateByUrl('/agentes');
      }, (err) => {
        Swal.fire('Error', err.error.msg, 'error' );
      });
  }

  cambiarImagen( file: File ) {
    this.imagenSubir = file;

    if ( !file ) { 
      return this.imgTemp = null;
    }

    const reader = new FileReader();
    reader.readAsDataURL( file );

    reader.onloadend = () => {
      this.imgTemp = reader.result;
    }

  }

}
