import { environment } from '../../environments/environment';

const base_url = environment.base_url;

export class Servicio {

    constructor(
        public name: string,
        public type: number,
        public description: string,
        public value: number,
        public status: number,
        public img?: string,
    ) {}

    get imagenUrl() {

        if ( !this.img ) {
            return `${ base_url }/upload/services/no-image`;
        }  else if ( this.img ) {
            return `${ base_url }/upload/services/${ this.img }`;
        } else {
            return `${ base_url }/upload/services/no-image`;
        }
        return 'asdas';
    }
}
