import { environment } from '../../environments/environment';

const base_url = environment.base_url;

export class Usuario {

    constructor(
        public name: string,
        public email: string,
        // public password?: string,
        //public google?: boolean,
        public type: number,
        public services: [],
        public uid?: string,
        public img?: string,
        public country?: string,
        public documentType?: string,
        public documentNumber?: string,
        public visa?: string,
        public status?: string,
    ) {}

    get imagenUrl() {

        if ( !this.img ) {
            return `${ base_url }/upload/users/no-image`;
        }  else if ( this.img ) {
            return `${ base_url }/upload/users/${ this.img }`;
        } else {
            return `${ base_url }/upload/users/no-image`;
        }
        return 'asdas';
    }
}

export enum UserType {
    Usuario = 1,
    Agente = 2,
    Cliente = 3,
}

