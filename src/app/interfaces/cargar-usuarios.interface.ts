import { Usuario } from '../models/usuario.model';

export interface CargarUsuario {
    users: any;
    total: number;
    usuarios: Usuario[];
}