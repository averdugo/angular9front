import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map} from 'rxjs/operators';

import { environment } from '../../environments/environment';
const base_url = environment.base_url;
import { Datainterface } from '../interfaces/datai.interface';

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  constructor(
    private http: HttpClient,
  ) { }
  get token(): string {
    return localStorage.getItem('token') || '';
  }
  get headers() {
    return {
      headers: {
        'x-token': this.token
      }
    };
  }

  all(){
    const url = `${ base_url }/faq`;
    return this.http.get<Datainterface>( url, this.headers )
            .pipe(map( resp => resp ));
  }

  create( formData ) {
    return this.http.post<Datainterface>(`${ base_url }/faq`, formData, this.headers );
  }
  findById(id) {
    const url = `${ base_url }/faq/${ id }`;
    return this.http.get<Datainterface>( url, this.headers );
  }
  edit(id, formData) {
    const url = `${ base_url }/faq/${ id }`;
    return this.http.put<Datainterface>( url, formData, this.headers );
  }

  delete(faq) {
    const url = `${ base_url }/faq/${ faq.uid }`;
    return this.http.delete<Datainterface>( url, this.headers );
  }

}
