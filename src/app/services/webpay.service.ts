import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, filter, scan } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as io from 'socket.io-client';
import * as Rx from 'rxjs';
@Injectable()

export class WebPayService {

    private socket;

    constructor(private _http: HttpClient){

    }

    connect(): Rx.Subject<MessageEvent>{
        this.socket = io(environment.ws_url);

        let observable = new Observable(observer => {
            this.socket.on('paid', (data) => {
                observer.next(data)
            });
            return () => {
                this.socket.disconnect();
            }
        });

        let observer= {
            next: (data: Object) => {
                this.socket.emit('paid', data)
            }
        };

        return Rx.Subject.create(observer, observable);
    }

    pagar(amount){
        const params = {
            "amount": amount
        };
        return this._http.post<any>(environment.ws_url + '/api/webpay/pagar', params, {headers:{
            'Content-Type': 'application/json'
        }})
       .pipe(
           map(res => res)
       );
    }
}