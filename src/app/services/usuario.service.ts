import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';

import { RegisterForm } from '../interfaces/register-form.interface';
import { LoginForm } from '../interfaces/login-form.interface';
import { CargarUsuario } from '../interfaces/cargar-usuarios.interface';

import { Usuario } from '../models/usuario.model';
import { Datainterface } from '../interfaces/datai.interface';

const base_url = environment.base_url;

declare const gapi: any;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  public auth2: any;
  public usuario: Usuario;

  constructor( 
    private http: HttpClient,
    private router: Router,
    private ngZone: NgZone
  ) {

    this.googleInit();
  }

  get token(): string {
    return localStorage.getItem('token') || '';
  }

  get uid():string {
    return this.usuario.uid || '';
  }

  get headers() {
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  googleInit() {

    return new Promise<void>( resolve => {
      gapi.load('auth2', () => {
        this.auth2 = gapi.auth2.init({
          client_id: '1045072534136-oqkjcjvo449uls0bttgvl3aejelh22f5.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin',
        });
        resolve();
      });
    });

  }

  logout() {
    localStorage.removeItem('token');

    this.auth2.signOut().then(() => {

      this.ngZone.run(() => {
        this.router.navigateByUrl('/login');
      });
    });

  }

  validarToken(): Observable<boolean> {
    return this.http.get(`${ base_url }/auth/renew`, {
      headers: {
        'x-token': this.token
      }
    }).pipe(
      map( (resp: any) => {
        const { email,  name, img,  type, services, uid } = resp.user;
        this.usuario = new Usuario( name, email, type, services, uid, img );
        localStorage.setItem('token', resp.token );
        return true;
      }),
      catchError( error => of(false) )
    );

  }


  crearUsuario( formData: RegisterForm ) {
    return this.http.post(`${ base_url }/auth/register`, formData )
      .pipe(
        tap( (resp: any) => {
          localStorage.setItem('token', resp.token )
        })
      );
  }

  actualizarPerfil( data: { email: string, nombre: string, role: string } ) {

    data = { ...data };
    return this.http.put(`${ base_url }/user/${ this.uid }`, data, this.headers );

  }

  login( formData: LoginForm ) {
    console.log('object');
    return this.http.post(`${ base_url }/auth`, formData )
      .pipe(
        tap( (resp: any) => {
          console.log(resp);
          localStorage.setItem('token', resp.token )
        })
      );

  }

  loginGoogle( token ) {
    return this.http.post(`${ base_url }/login/google`, { token } )
    .pipe(
      tap( (resp: any) => {
        localStorage.setItem('token', resp.token )
      })
    );

  }
  cargarUsuarios( desde: number = 0, type ) {
    const url = `${ base_url }/user/byType/${type}?desde=${ desde }`;
    return this.http.get<CargarUsuario>( url, this.headers ).pipe(
      map( resp => {
        const usuarios = resp.users.map( 
          user => new Usuario(user.name, user.email, user.type, [], user.uid, user.img )
        );
        return {
          total: resp.total,
          usuarios
        };
      })
    );
  }


  eliminarUsuario( usuario: Usuario ) {
      const url = `${ base_url }/usuarios/${ usuario.uid }`;
      return this.http.delete( url, this.headers );
  }

  guardarUsuario( usuario: Usuario ) {
    return this.http.put(`${ base_url }/usuarios/${ usuario.uid }`, usuario, this.headers );

  }

  findById(id) {
    const url = `${ base_url }/user/${ id }`;
    return this.http.get<any>( url, this.headers );
  }
  edit(id, formData) {
    const url = `${ base_url }/user/${ id }`;
    return this.http.put<Datainterface>( url, formData, this.headers );
  }

}
