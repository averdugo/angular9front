import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';
const base_url = environment.base_url;
import { Datainterface } from '../interfaces/datai.interface';


@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  constructor(
    private http: HttpClient,
  ) { }

  get token(): string {
    return localStorage.getItem('token') || '';
  }
  get headers() {
    return {
      headers: {
        'x-token': this.token
      }
    };
  }

  all(){
    const url = `${ base_url }/service`;
    return this.http.get<Datainterface>( url, this.headers )
            .pipe(map( resp => resp ));
  }

  create( formData ) {
    return this.http.post<Datainterface>(`${ base_url }/service`, formData, this.headers );
  }
  findById(id) {
    const url = `${ base_url }/service/${ id }`;
    return this.http.get<Datainterface>( url, this.headers );
  }
  edit(id, formData) {
    const url = `${ base_url }/service/${ id }`;
    return this.http.put<Datainterface>( url, formData, this.headers );
  }

  delete(servicio) {
    const url = `${ base_url }/service/${ servicio.uid }`;
    return this.http.delete<Datainterface>( url, this.headers );
  }
}
