import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menu: any[] = [
    {
      titulo: 'Servicios',
      icono: 'mdi mdi-clipboard',
      submenu: [
        { titulo: 'Listado', url: '/servicios' },
        { titulo: 'Crear', url: '/servicios/crear' },
        { titulo: 'Reportes', url: '/grafica1' },
      ]
    },
    {
      titulo: 'Usuarios',
      icono: 'mdi mdi-account',
      submenu: [
        { titulo: 'Clientes', url: '/clientes' },
        { titulo: 'Agentes', url: '/agentes' },
        { titulo: 'Usuarios', url: '/usuarios' },
      ]
    },
    {
      titulo: 'Reportes',
      icono: 'mdi mdi-folder-lock-open',
      submenu: [
        { titulo: 'Gráficas', url: '/grafica1' },
      ]
    },
    {
      titulo: 'Faq',
      icono: 'mdi mdi-help',
      submenu: [
        { titulo: 'Listar', url: '/faqs' },
        { titulo: 'Crear', url: '/faqs/crear' },
      ]
    },
    {
      titulo: 'Chats',
      icono: 'mdi mdi-folder-lock-open',
      submenu: [
        { titulo: 'Listar', url: '/chats' },
      ]
    },
  ];

  constructor() { }
}
